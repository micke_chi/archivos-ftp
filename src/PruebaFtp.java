import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

public class PruebaFtp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String server = "chapurcloud.chapur";
		int port = 21;
		String user = "carlos.marcano";
		String pass = "CarlosMAR63";
		FTPClient ftpClient = new FTPClient();
		
		try {
			ftpClient.connect(server, port);
			ftpClient.login(user, pass);
			ftpClient.enterLocalPassiveMode();
			
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			
			//Subiendo un archivo al FTP
			
			File firstLocalFile = new File("C:/workspace/recursos/archivoLocal2.txt");
			String firstRemoteFile = "/Desarrollo TI/pruebasftp/archivoRemoto2.txt";
			InputStream inputStream = new FileInputStream(firstLocalFile);
			
			System.out.println("Iniciando la subida de archivos");
			boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
			inputStream.close();
			if(done) {
				System.out.println("Se finalizo la subida de archivos");
			}
			
			
			//Bajando archivo del ftp
			/*
			String remoteFile1 = "/Desarrollo TI/pruebasftp/archivoRemoto1.txt";
			File downloadfile1 = new File("C:/workspace/recursos/archivoLocal1.txt");
			OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadfile1));
			
			System.out.println("Iniciando la descarga de archivos");
			boolean done = ftpClient.retrieveFile(remoteFile1, outputStream1);
			outputStream1.close();
			if(done) {
				System.out.println("Se finalizo la descarga de archivos");
			}
			*/
			
		}
		catch (IOException e) {
			// TODO: handle exception
			System.out.println("Error" + e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if(ftpClient.isConnected()) {
					ftpClient.logout();
					ftpClient.disconnect();
				}
			} catch (Exception e2) {
				System.out.println("Error" + e2.getMessage());
				e2.printStackTrace();
				// TODO: handle exception
			}
		}

	}

}
